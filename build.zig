const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const eigen = b.addStaticLibrary(.{
        .name = "eigen",
        .target = target,
        .optimize = optimize,
    });
    b.installArtifact(eigen);
    eigen.addCSourceFiles(&.{
        "src/binary_library.cpp",
    }, &.{
        "-O2", "-msse2",
    });
    eigen.addIncludePath("src");
    eigen.addIncludePath(".");
    eigen.linkLibCpp();

    const example = b.addExecutable(.{
        .name = "example",
        .root_source_file = .{ .path = "src/example.zig" },
        .target = target,
        .optimize = optimize,
    });
    example.addIncludePath("src");
    example.linkLibCpp();
    example.linkLibrary(eigen);
    const run_example = b.addRunArtifact(example);
    run_example.has_side_effects = true;
    const run_example_step = b.step("run-example", "run the example exe");
    run_example_step.dependOn(&run_example.step);
}

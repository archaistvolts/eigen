const std = @import("std");

const c = @cImport(@cInclude("binary_library.h"));
const stdout = std.io.getStdOut().writer();

fn demo_MatrixXd() !void {
    _ = try stdout.write("*** demo_MatrixXd ***\n");

    const matrix1 = c.MatrixXd_new(3, 3);
    c.MatrixXd_set_zero(matrix1);
    c.MatrixXd_set_coeff(matrix1, 0, 1, 2.5);
    c.MatrixXd_set_coeff(matrix1, 1, 0, 1.4);
    _ = try stdout.write("Here is matrix1:\n");
    c.MatrixXd_print(matrix1);

    const matrix2 = c.MatrixXd_new(3, 3);
    c.MatrixXd_multiply(matrix1, matrix1, matrix2);
    _ = try stdout.write("Here is matrix1*matrix1:\n");
    c.MatrixXd_print(matrix2);

    c.MatrixXd_delete(matrix1);
    c.MatrixXd_delete(matrix2);
}

fn print_array(array: [*]f64, n: i32) void {
    const m = c.Map_MatrixXd_new(array, 1, n);
    c.Map_MatrixXd_print(m);
    c.Map_MatrixXd_delete(m);
}

fn demo_Map_MatrixXd() !void {
    var array: [5]f64 = undefined;

    _ = try stdout.write("*** demo_Map_MatrixXd ***\n");

    for (array, 0..) |_, i| array[i] = @intToFloat(f64, i);
    _ = try stdout.write("Initially, the array is:\n");
    print_array(&array, 5);

    var map = c.Map_MatrixXd_new(&array, 5, 1);
    c.Map_MatrixXd_add(map, map, map);
    c.Map_MatrixXd_delete(map);

    _ = try stdout.write("Now the array is:\n");
    print_array(&array, 5);
}

pub fn main() !void {
    try demo_MatrixXd();
    try demo_Map_MatrixXd();
}
